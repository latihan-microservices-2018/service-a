package com.example.servicea;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableFeignClients
@RestController
@EnableCircuitBreaker
public class ServiceAApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceAApplication.class);

    @Autowired
    private ServiceB serviceB;

    @GetMapping("/foo")
    public Map<String, String> cInfo(HttpServletRequest request) {
        LOGGER.info("Menerima request di {} : {}", request.getLocalAddr(), request.getLocalPort());
        return serviceB.serviceBInfo();
    }

    public static void main(String[] args) {
        SpringApplication.run(ServiceAApplication.class, args);
    }
}

@FeignClient(name = "serviceB", fallback = ServiceBFallback.class)
interface ServiceB {

    @GetMapping("/foo/c")
    public Map<String, String> serviceBInfo();
}

@Component
class ServiceBFallback implements ServiceB {

    @Override
    public Map<String, String> serviceBInfo() {
        Map<String, String> hasil = new HashMap<>();
        hasil.put("status", "b not available");
        return hasil;
    }

}
